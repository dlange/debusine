#!/usr/bin/env python3

# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.
"""
Debusine integration tests.

Test Lintian task.
"""

import logging
import textwrap
import unittest

from utils.client import Client
from utils.common import Configuration, launch_tests
from utils.integration_test_helpers_mixin import IntegrationTestHelpersMixin
from utils.server import DebusineServer

logger = logging.getLogger(__name__)


class IntegrationTaskLintianTests(
    IntegrationTestHelpersMixin, unittest.TestCase
):
    """
    Integration test for the Lintian task.

    These tests assume:
    - debusine-server is running
    - debusine-worker is running (connected to the server)
    - debusine-client is correctly configured

    debusine-worker is not started (neither restarted) during the tests.
    """

    TASK_NAME = "lintian"

    def setUp(self):
        """Initialize test."""
        # If debusine-server or nginx was launched just before the
        # integration-tests.py is launched the debusine-server might not be
        # yet available. Let's wait for the debusine-server to be
        # reachable if it's not ready yet
        self.assertTrue(
            DebusineServer.wait_for_server_ready(),
            'debusine-server should be available (in '
            f'{Configuration.BASE_URL}) before the integration tests are ran',
        )

        self._environment_id = self.find_system_image(suite="bookworm")

    def test_lintian_input_source_artifact(self):
        """Create lintian job: input is "hello" source package."""
        source_artifact_id = self.create_artifact_source("hello")
        upload_artifact_id = self.create_artifact_upload(["hello"])

        task_data = textwrap.dedent(
            f'''\
            input:
              source_artifact_id: {source_artifact_id}
              binary_artifacts_ids: [{upload_artifact_id}]
            environment_id: {self._environment_id}
            '''
        )

        # Client submits a work-request
        work_request_id = Client.execute_command(
            "create-work-request", self.TASK_NAME, stdin=task_data
        )["work_request_id"]

        # The worker should get the new work request and start executing it,
        # wait for success
        self.assertTrue(
            Client.wait_for_work_request_completed(work_request_id, "success")
        )

        show_work_request = Client.execute_command(
            "show-work-request", work_request_id
        )

        debian_lintian_artifacts = 0

        for artifact in show_work_request["artifacts"]:
            if artifact["category"] == "debian:lintian":
                debian_lintian_artifacts += 1

                # Expected analysis.json and lintian.txt
                self.assertCountEqual(
                    artifact["files"].keys(), ["analysis.json", "lintian.txt"]
                )

        # 3 artifacts: for source, binary_any, binary_all
        self.assertEqual(debian_lintian_artifacts, 3)


if __name__ == '__main__':
    launch_tests("Task lintian integration tests for debusine")
