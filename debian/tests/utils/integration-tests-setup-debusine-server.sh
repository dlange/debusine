#!/bin/sh

set -e

# Setup debusine-server with similar steps as an admin would do:
# Creates the database, migrate, set up nginx, allowed-hosts
#
# It assumes that postgres is already running

# Setup postgresql database and user
sudo -u postgres createuser debusine-server || true
sudo -u postgres createdb --owner debusine-server debusine || true

# Execute migration
sudo -u debusine-server debusine-admin migrate

# Set up nginx
cp /usr/share/doc/debusine-server/examples/nginx-vhost.conf \
	/etc/nginx/sites-available/debusine.example.net

ln -sf /etc/nginx/sites-available/debusine.example.net \
	/etc/nginx/sites-enabled

rm -f /etc/nginx/sites-enabled/default

systemctl restart debusine-server
systemctl restart nginx
