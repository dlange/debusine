# PYTHONDEVMODE to enable PYTHONASYNCDEBUG=1 and other flags
PYTHON_ENVIRONMENT := PYTHONASYNCDEBUG=1 PYTHONDEBUG=1

ifeq ($(VERBOSE),1)
    VERBOSE_OPTION_DJANGO_TESTS:= -v 2
    VERBOSE_OPTION_TESTS := -v
else
    VERBOSE_OPTION_DJANGO_TESTS :=
    VERBOSE_OPTION_TESTS :=
endif

# Quick sanity check to run before each commit
check:
	tox -e pyupgrade,black,flake8,codespell,mypy,shellcheck

# Reformat the code and show changes made by black
black:
	tox -e format

coverage:
	python3 -m coverage erase
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p ./manage.py test debusine.db debusine.server debusine.web debusine.project debusine.test $(VERBOSE_OPTION_DJANGO_TESTS)
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.artifacts $(VERBOSE_OPTION_TESTS)
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.client $(VERBOSE_OPTION_TESTS)
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.tasks $(VERBOSE_OPTION_TESTS)
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.worker $(VERBOSE_OPTION_TESTS)
	$(PYTHON_ENVIRONMENT) python3 -m coverage run -p -m unittest discover debusine.utils $(VERBOSE_OPTION_TESTS)
	python3 -m coverage combine
	python3 -m coverage html

.PHONY: check black coverage
