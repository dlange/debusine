# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""URLs for downloading artifacts."""

from django.urls import path

from debusine.web.views import CreateArtifactView, DownloadPathView

app_name = "artifacts"

urlpatterns = [
    path(
        "<int:artifact_id>/<path:path>",
        DownloadPathView.as_view(),
        name="detail-path",
    ),
    path(
        "<int:artifact_id>/",
        DownloadPathView.as_view(),
        name="detail",
    ),
    path("create/", CreateArtifactView.as_view(), name="create"),
]
