# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""The collection manager for debian:environments collections."""

from typing import Optional

from django.db import IntegrityError
from django.db.models import Q
from django.utils import timezone

from debusine.db.models import Artifact, Collection, CollectionItem, User
from debusine.server.collections.base import (
    CollectionManagerInterface,
    ItemAdditionError,
    ItemRemovalError,
)


class DebianEnvironmentsManager(CollectionManagerInterface):
    """Manage collection of category debian:environments."""

    COLLECTION_CATEGORY = "debian:environments"
    VALID_ARTIFACT_CATEGORIES = frozenset(
        {
            "debian:system-tarball",
            "debian:system-image",
        }
    )

    def do_add_artifact(
        self,
        artifact: Artifact,
        *,
        user: User,
    ) -> CollectionItem:
        """Add the artifact into the managed collection."""
        artifact_data = artifact.data

        codename = artifact_data["codename"]
        architecture = artifact_data["architecture"]
        variant = artifact_data["variant"]

        data = {
            "codename": codename,
            "architecture": architecture,
            "variant": variant,
        }

        name = ":".join([codename, architecture, variant])

        manager = CollectionItem.objects
        try:
            return manager.create_from_artifact(  # type: ignore[attr-defined]
                artifact,
                parent_collection=self.collection,
                name=name,
                data=data,
                created_by_user=user,
            )
        except IntegrityError as exc:
            raise ItemAdditionError(str(exc))

    def do_remove_artifact(
        self,
        artifact: Artifact,
        *,
        user: Optional[User] = None,
    ):
        """Remove the artifact from the collection."""
        # Retention logic will remove the artifact, now only marked as
        # removed
        CollectionItem.objects.filter(
            artifact=artifact, parent_collection=self.collection
        ).update(removed_by_user=user, removed_at=timezone.now())

    def do_add_collection(
        self,
        collection: Collection,  # noqa: U100
        *,
        user: User,  # noqa: U100
    ) -> CollectionItem:
        """Raise ItemAdditionError: collection does not have collections."""
        raise ItemAdditionError(
            f'Cannot add collections into "{self.COLLECTION_CATEGORY}"'
        )

    def do_remove_collection(
        self,
        collection: Collection,  # noqa: U100
        *,
        user: Optional[User] = None,  # noqa: U100
    ):
        """Raise ItemRemovalError: collection cannot be removed."""
        raise ItemRemovalError(
            f'Cannot remove collections from "{self.COLLECTION_CATEGORY}"'
        )

    def lookup(self, query: str) -> Optional[CollectionItem]:
        """
        Return one CollectionItem based on the query.

        :param query: Examples "tarball:CODENAME:ARCH",
          "tarball:CODENAME:ARCH:VARIANT". If more than one possible
          CollectionItem match the query (e.g. multiple variants): return
          the most recently added one.
        """
        if query.count(":") not in (2, 3):
            raise LookupError(f'Unexpected number of : in "{query}"')

        format_, codename, arch, *variant_or_empty_list = query.split(":")
        variant = variant_or_empty_list[0] if variant_or_empty_list else None

        if format_ == "tarball":
            category = "debian:system-tarball"
        elif format_ == "image":
            category = "debian:system-image"
        else:
            raise LookupError(f'Unexpected format: "{format_}" in "{query}"')

        query_filter = (
            Q(parent_collection=self.collection)
            & Q(category=category)
            & Q(data__codename=codename)
            & Q(data__architecture=arch)
            & Q(child_type=CollectionItem.Types.ARTIFACT)
            & Q(removed_at__isnull=True)
        )

        if variant is not None:
            query_filter &= Q(data__variant=variant)

        try:
            return CollectionItem.objects.filter(query_filter).latest(
                "created_at"
            )
        except CollectionItem.DoesNotExist:
            return None
