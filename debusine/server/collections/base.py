# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Interface for collections."""
from abc import ABC, abstractmethod
from typing import Optional

from debusine.db.models import (
    Artifact,
    Collection,
    CollectionItem,
    User,
)


class ItemAdditionError(Exception):
    """
    Raised if the object cannot be added to the collection.

    For example: the object's category is not valid for the collection,
    or adding this object breaks some collection constraints.
    """


class ItemRemovalError(Exception):
    """
    Raised if the object cannot be removed from the collection.

    For example: removing this Item breaks a collection constraint.
    """


class CollectionManagerInterface(ABC):
    """Interface to manage a collection."""

    # Category of the managed collection
    COLLECTION_CATEGORY: str = ""

    # Valid categories of the artifacts added in the collection
    VALID_ARTIFACT_CATEGORIES: frozenset[str] = frozenset()

    # Valid categories of the collections added in the collection
    VALID_COLLECTION_CATEGORIES: frozenset[str] = frozenset()

    def __init__(self, collection: Collection):
        """
        Instantiate collection manager.

        :param collection: collection to be managed
        """
        if collection.category != self.COLLECTION_CATEGORY:
            raise ValueError(
                f'{self.__class__.__name__} cannot manage '
                f'"{collection.category}" category'
            )

        # collection that the items will be added / removed / looked up
        self.collection = collection

    def add_artifact(self, artifact: Artifact, *, user: User) -> CollectionItem:
        """
        Add the artifact to the managed collection.

        Verify that the artifact can be added (e.g. artifact category is valid)
        and call self.do_add_artifact(artifact, user=user).

        :param artifact: artifact to add
        :param user: user adding the artifact to the collection
        """
        if artifact.category not in self.VALID_ARTIFACT_CATEGORIES:
            raise ItemAdditionError(
                f'Artifact category "{artifact.category}" '
                f'not supported by the collection'
            )

        return self.do_add_artifact(artifact, user=user)

    def add_collection(
        self, collection: Collection, *, user: User
    ) -> CollectionItem:
        """
        Add the collection to the managed collection.

        Verify that the collection can be added (e.g. collection category
        is valid) and call
        self.do_add_artifact(artifact, user=user).

        :param collection: collection to add
        :param user: user adding the collection
        """
        if collection.category not in self.VALID_COLLECTION_CATEGORIES:
            raise ItemAdditionError(
                f'Collection category "{collection.category}" '
                f'not supported by the collection'
            )

        return self.do_add_collection(collection, user=user)

    def remove_artifact(
        self,
        artifact: Artifact,
        *,
        user: Optional[User] = None,
    ):
        """
        Remove the artifact from the managed collection.

        Verify that the artifact can be removed and call
        self.do_remove_artifact(artifact, user=user).

        :param artifact: artifact being removed from the collection
        :param user: user removing the artifact from the collection
        """
        # Check that the artifact can be removed
        # raise ItemRemovalError() if needed
        self.do_remove_artifact(artifact, user=user)

    def remove_collection(
        self,
        collection: Collection,
        *,
        user: Optional[User],
    ):
        """
        Remove the collection from the managed collection.

        :param collection: collection being removed from the collection
        :param user: user removing the collection from the collection
        """
        # Check that the collection can be removed
        # raise ItemRemovalError() if needed
        self.do_remove_collection(collection, user=user)

    @abstractmethod
    def do_add_artifact(
        self, artifact: Artifact, *, user: User
    ) -> CollectionItem:
        """
        Add the artifact into the managed collection.

        Called by add_artifact().

        :raise ItemAdditionError: the artifact cannot be added: breaks
          collection's constraints.
        """

    @abstractmethod
    def do_remove_artifact(
        self,
        artifact: Artifact,
        *,
        user: Optional[User] = None,
    ):
        """
        Remove the artifact from the managed collection.

        Called by remove_artifact().

        :raise: ItemRemovalError: the artifact cannot be removed: breaks
          collection's constraints.
        """

    @abstractmethod
    def do_add_collection(
        self, collection: Collection, *, user: User
    ) -> CollectionItem:
        """
        Add the collection into the managed collection.

        Called by add_collection().

        :raise ItemAdditionError: the collection cannot be added: breaks
          collection's constraints.
        """

    @abstractmethod
    def do_remove_collection(
        self,
        collection: Collection,
        *,
        user: Optional[User] = None,
    ):
        """
        Remove the collection from the managed collection.

        Called by remove_collection().

        :raise: ItemRemovalError: the collection cannot be removed: breaks
          collection's constraints.
        """

    @abstractmethod
    def lookup(self, query: str) -> Optional[CollectionItem]:
        """
        Return one CollectionItem.

        For example, a caller could do:
        collection_manager.lookup("tarball:bookworm:amd64:apt")

        collection_manager.lookup("image:trixie:amd64")
        """  # noqa: RST301, RST201, RST203
