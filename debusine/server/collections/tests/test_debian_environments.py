# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the DebianEnvironmentsManager."""
import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from debusine.db.models import (
    Collection,
    CollectionItem,
    default_workspace,
)
from debusine.server.collections import (
    DebianEnvironmentsManager,
    ItemAdditionError,
    ItemRemovalError,
)
from debusine.test import TestHelpersMixin


class DebianEnvironmentsManagerTests(TestHelpersMixin, TestCase):
    """Tests for DebianEnvironmentsManager."""

    def setUp(self):
        """Set up tests."""
        self.user = get_user_model().objects.create_user(
            username="John", email="john@example.org"
        )

        self.workspace = default_workspace()

        self.collection = Collection.objects.create(
            name="Debian",
            category="debian:environments",
            workspace=self.workspace,
        )

        self.manager = DebianEnvironmentsManager(collection=self.collection)

    def test_init_wrong_collection_category_raise_value_error(self):
        """Init raise ValueError: wrong collection category."""
        category = "debian:something-else"
        collection = Collection.objects.create(
            name="Name is not used",
            category=category,
            workspace=self.workspace,
        )

        msg = f'^DebianEnvironmentsManager cannot manage "{category}" category$'

        with self.assertRaisesRegex(ValueError, msg):
            DebianEnvironmentsManager(collection)

    def test_do_add_artifact(self):
        """Test do_add_artifact adds the artifact."""
        data = {
            "codename": "bookworm",
            "architecture": "amd64",
            "variant": "apt",
        }
        artifact, _ = self.create_artifact(
            category="debian:system-tarball",
            data={**data, "with_dev": True},
        )

        collection_item = self.manager.add_artifact(artifact, user=self.user)

        collection_item.refresh_from_db()

        self.assertEqual(collection_item.name, "bookworm:amd64:apt")
        self.assertEqual(collection_item.data, data)

    def test_do_add_artifact_raise_item_addition_error(self):
        """Test do_add_artifact raise error: duplicated CollectionItem data."""
        data = {
            "codename": "bookworm",
            "architecture": "amd64",
            "variant": "apt",
        }
        artifact_1, _ = self.create_artifact(
            category="debian:system-tarball", data=data
        )

        artifact_2, _ = self.create_artifact(
            category="debian:system-tarball",
            data=data,
        )

        self.manager.add_artifact(artifact_1, user=self.user)

        with self.assertRaisesRegex(
            ItemAdditionError, "db_collectionitem_unique_active_name"
        ):
            self.manager.add_artifact(artifact_2, user=self.user)

    def test_do_remove_artifact(self):
        """Test do_remove_artifact removes the artifact."""
        data = {
            "codename": "bookworm",
            "architecture": "amd64",
            "variant": "apt",
        }

        artifact, _ = self.create_artifact(
            category="debian:system-tarball",
            data={**data, "with_dev": True},
        )

        collection_item = self.manager.add_artifact(artifact, user=self.user)

        # Test removing the artifact from the collection
        self.manager.remove_artifact(artifact, user=self.user)

        collection_item.refresh_from_db()

        # The artifact is not removed yet (retention period applies)
        self.assertEqual(collection_item.artifact, artifact)

        self.assertEqual(collection_item.removed_by_user, self.user)
        self.assertIsInstance(collection_item.removed_at, datetime.datetime)

    def test_do_remove_collection_raise_item_removal_error(self):
        """
        Test do_remove_collection raise ItemRemovalError.

        No Collections can be added or removed in
        debian:environments collection.
        """
        msg = (
            f'^Cannot remove collections from '
            f'"{self.manager.COLLECTION_CATEGORY}"$'
        )
        collection = Collection.objects.create(
            name="Some-collection",
            category="Some category",
            workspace=self.workspace,
        )

        with self.assertRaisesRegex(ItemRemovalError, msg):
            self.manager.do_remove_collection(collection, user=self.user)

    def test_do_add_collection_raise_item_removal_error(self):
        """
        Test do_add_collection raise ItemAdditionError.

        No Collections can be added or removed in
        debian:environments collection.
        """
        msg = (
            f'^Cannot add collections into '
            f'"{self.manager.COLLECTION_CATEGORY}"$'
        )
        collection = Collection.objects.create(
            name="Some-collection",
            category="Some category",
            workspace=self.workspace,
        )

        with self.assertRaisesRegex(ItemAdditionError, msg):
            self.manager.do_add_collection(collection, user=self.user)

    def test_lookup_not_enough_colons_raise_lookup_error(self):
        """Test lookup raise LookupError: unexpected number of colons."""
        msg = '^Unexpected number of : in "a:b"'

        with self.assertRaisesRegex(LookupError, msg):
            self.manager.lookup("a:b")

    def test_lookup_unexpected_format_raise_lookup_error(self):
        """Test lookup raise LookupError: invalid format."""
        msg = '^Unexpected format: "targz" in "targz:bookworm:amd64"$'

        with self.assertRaisesRegex(LookupError, msg):
            self.manager.lookup("targz:bookworm:amd64")

    def test_lookup_return_empty_lists(self):
        """Test lookup return empty lists."""
        self.assertIsNone(self.manager.lookup("tarball:bookworm:amd64"))

    def test_lookup_return_matching_collection_item(self):
        """Test lookup return artifacts."""
        artifact_1, _ = self.create_artifact(
            category="debian:system-tarball",
            data={
                "codename": "bookworm",
                "architecture": "amd64",
                "variant": "apt",
            },
        )
        item_1 = self.manager.add_artifact(artifact_1, user=self.user)

        artifact_2, _ = self.create_artifact(
            category="debian:system-tarball",
            data={
                "codename": "bookworm",
                "architecture": "amd64",
                "variant": "buildd",
            },
        )
        item_2 = self.manager.add_artifact(artifact_2, user=self.user)

        artifact_3, _ = self.create_artifact(
            category="debian:system-image",
            data={
                "codename": "bullseye",
                "architecture": "amd64",
                "variant": "custom",
            },
        )
        item_3 = self.manager.add_artifact(artifact_3, user=self.user)

        # Next one is not returned by lookup because is removed.
        artifact_4, _ = self.create_artifact(
            category="debian:system-image",
            data={
                "codename": "bullseye",
                "architecture": "amd64",
                "variant": "apt",
            },
        )
        item_4 = self.manager.add_artifact(artifact_4, user=self.user)
        item_4.removed_at = timezone.now()
        item_4.save()

        # CollectionItem of type BARE should not exist in this collection
        # (the manager does not allow to add it). Add one to see that is
        # filtered out and not included in the result / cause problems
        CollectionItem.objects.create(
            child_type=CollectionItem.Types.BARE,
            created_by_user=self.user,
            parent_collection=self.collection,
            category="system:tarball",
            name="something",
            data={"codename": "bookworm", "arch": "amd64", "variant": "apt"},
        )

        # item_2 because is created_at after item_1
        self.assertEqual(self.manager.lookup("tarball:bookworm:amd64"), item_2)

        # item_1 because the variant is specified
        self.assertEqual(
            self.manager.lookup("tarball:bookworm:amd64:apt"), item_1
        )

        self.assertEqual(self.manager.lookup("image:bullseye:amd64"), item_3)
