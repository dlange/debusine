# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for generic notifications."""

import logging
from typing import Any, Optional
from unittest import mock

from asgiref.sync import sync_to_async

from django.core import mail
from django.core.mail import EmailMessage
from django.test import TestCase

from debusine.db.models import (
    FileStore,
    NotificationChannel,
    Token,
    WorkRequest,
    Worker,
    Workspace,
)
from debusine.server import notifications
from debusine.tasks.models import NotificationDataEmail, TaskNotification
from debusine.test import TestHelpersMixin
from debusine.test.django import ChannelsHelpersMixin


class NotifyWorkerTokenDisabledTests(ChannelsHelpersMixin, TestCase):
    """Tests for notify_worker_token_disabled."""

    def setUp(self):
        """Initialize test."""
        self.token = Token.objects.create()

    async def test_message_is_not_sent(self):
        """Test Token with an associated worker: message is sent."""
        channel = await self.create_channel(self.token.key)

        await sync_to_async(self.token.disable)()

        await self.assert_channel_nothing_received(channel)

    async def test_message_is_sent(self):
        """Test Token with an associated worker: message is sent."""
        await sync_to_async(Worker.objects.create_with_fqdn)(
            fqdn="debusine", token=self.token
        )

        channel = await self.create_channel(self.token.key)

        await sync_to_async(self.token.disable)()

        await self.assert_channel_received(channel, {"type": "worker.disabled"})


class NotifyWorkRequestAssigned(
    ChannelsHelpersMixin, TestHelpersMixin, TestCase
):
    """Tests for notify_work_request_assigned."""

    def setUp(self):
        """Initialize test."""
        workspace, _ = Workspace.objects.get_or_create(
            name="Tests", default_file_store=FileStore.default()
        )
        token = self.create_token_enabled(with_user=True)
        self.work_request = WorkRequest(
            workspace=workspace, created_by=token.user
        )

    def test_message_is_not_sent(self):
        """Test WorkRequest.assign_worker(None) is a no-operation."""
        # Asserts that no exception is raised. notify_work_request_assigned
        # is checking that worker is None and not trying to access the
        # worker's token.
        self.work_request.assign_worker(None)

    async def test_message_is_sent(self):
        """Test WorkRequest.assign_worker(worker) sends msg to the channel."""
        token = await sync_to_async(Token.objects.create)()
        worker = await sync_to_async(Worker.objects.create_with_fqdn)(
            "debusine", token=token
        )

        channel = await self.create_channel(token.key)

        await sync_to_async(self.work_request.assign_worker)(worker)

        await self.assert_channel_received(
            channel, {"type": "work_request.assigned"}
        )

    def test_internal_worker(self):
        """
        WorkRequest.assign_worker sends no message for internal workers.

        These are assumed to notify the worker of work to do via some other
        mechanism (e.g. a Celery queue).
        """
        worker = Worker.objects.get_or_create_celery()

        # Asserts that no exception is raised.
        self.work_request.assign_worker(worker)


class NotifyWorkRequestCompletedTests(TestHelpersMixin, TestCase):
    """Test notify_work_request_completed function."""

    def assert_send_notification(
        self,
        status: WorkRequest.Results,
        expected_subject: str,
        *,
        expected_to: Optional[list[str]] = None,
        notification_data: Optional[NotificationDataEmail] = None,
        expected_cc: Optional[list[str]] = None,
    ):
        """Assert WorkRequest completed in failure or error."""
        notification_channel_name = "deblts-email"

        task_data: dict[str, Any] = {
            "notifications": {
                "on_failure": [
                    {
                        "channel": notification_channel_name,
                    }
                ]
            }
        }
        if notification_data is not None:
            task_data["notifications"]["on_failure"][0][
                "data"
            ] = notification_data.dict()

        work_request: WorkRequest = self.create_work_request(
            status=WorkRequest.Statuses.RUNNING, task_data=task_data
        )

        if expected_to is None:
            to = ["recipient@example.com"]
        else:
            to = expected_to

        notification_channel_data = {
            "from": "sender@debusine.example.org",
            "to": to,
        }

        NotificationChannel.objects.create(
            name=notification_channel_name,
            method=NotificationChannel.Methods.EMAIL,
            data=notification_channel_data,
        )

        work_request.mark_completed(status)

        self.assertEqual(len(mail.outbox), 1)

        sent_mail = mail.outbox[0]

        self.assertEqual(
            sent_mail.subject,
            expected_subject.format(work_request_id=work_request.id),
        )
        self.assertEqual(sent_mail.body, "Sent by debusine")

        self.assertEqual(sent_mail.to, to)

        if expected_cc is None:
            expected_cc = []

        self.assertEqual(sent_mail.cc, expected_cc)

    def test_send_notification_on_success(self) -> None:
        """No notification emails are sent in case of success."""
        notification_channel_name = "deblts-email"
        to = ["a@example.com"]

        task_data = {
            "notifications": {
                "on_failure": [
                    {
                        "channel": notification_channel_name,
                        "data": {
                            "to": to,
                            "subject": "Bad news: $work_request_id completed "
                            "in $work_request_result $invalid_variable",
                        },
                    }
                ]
            }
        }

        work_request: WorkRequest = self.create_work_request(
            status=WorkRequest.Statuses.RUNNING, task_data=task_data
        )

        notification_channel_data = {
            "from": "sender@debusine.example.org",
            "to": to,
        }

        NotificationChannel.objects.create(
            name=notification_channel_name,
            method=NotificationChannel.Methods.EMAIL,
            data=notification_channel_data,
        )

        work_request.mark_completed(WorkRequest.Results.SUCCESS)

        self.assertEqual(len(mail.outbox), 0)

    def test_send_notification_on_failure(self):
        """
        One notification email is sent.

        The subject {invalid_variable} is not replaced
        """
        to = ["a@example.com"]
        self.assert_send_notification(
            WorkRequest.Results.FAILURE,
            notification_data=NotificationDataEmail(
                to=to,
                subject=(
                    "Bad news: $work_request_id completed "
                    "in $work_request_result $invalid_variable"
                ),
            ),
            expected_to=to,
            expected_subject="Bad news: {work_request_id} completed in failure "
            "$invalid_variable",
        )

    def test_send_notification_on_error(self):
        """One notification email is sent."""
        to = ["a@example.com"]
        cc = ["cc@example.com"]

        self.assert_send_notification(
            WorkRequest.Results.ERROR,
            notification_data=NotificationDataEmail(to=to, cc=cc),
            expected_to=to,
            expected_cc=cc,
            expected_subject="WorkRequest {work_request_id} completed in error",
        )

    def test_send_notification_on_error_without_data(self):
        """Mail is sent, WorkRequest notification's data is not provided."""
        self.assert_send_notification(
            WorkRequest.Results.ERROR,
            expected_subject="WorkRequest {work_request_id} completed in error",
        )

    def test_no_notification_is_sent(self) -> None:
        """No mail notification is sent."""
        work_request: WorkRequest = self.create_work_request(
            status=WorkRequest.Statuses.RUNNING,
            task_data={"notifications": None},
        )
        work_request.mark_completed(WorkRequest.Results.FAILURE)
        self.assertEqual(len(mail.outbox), 0)

    def test_log_notification_channel_does_not_exist(self) -> None:
        """Log contains debug error that notification channel does not exist."""
        channel_name = "does-not-exist"
        task_data = {
            "notifications": {
                "on_failure": [
                    {
                        "channel": channel_name,
                        "data": {"to": ["a@example.com"]},
                    }
                ]
            }
        }
        work_request: WorkRequest = self.create_work_request(
            status=WorkRequest.Statuses.RUNNING, task_data=task_data
        )

        expected_log = (
            f"WorkRequest {work_request.id}: on_failure notification cannot be "
            f"sent: NotificationChannel {channel_name} does not exist"
        )
        with self.assertLogsContains(
            expected_log, logger="debusine", level=logging.DEBUG
        ):
            work_request.mark_completed(WorkRequest.Results.FAILURE)

    def test_send_work_request_completed_fail_logs(self):
        """Send email fails: log failure."""
        work_request = self.create_work_request()
        notification_channel = NotificationChannel.objects.create(
            name="deblts-email",
            method=NotificationChannel.Methods.EMAIL,
            data={
                "from": "sender@debusine.example.org",
                "to": ["rcpt@example.com"],
            },
        )
        work_request_notification = TaskNotification(channel="test")
        body = "Not used"

        email_send_patch = mock.patch.object(
            EmailMessage, "send", return_value=0
        )
        mocked = email_send_patch.start()
        self.addCleanup(email_send_patch.stop)

        expected_log = (
            f"NotificationChannel email notification for "
            f"WorkRequest {work_request.id} failed sending"
        )
        with self.assertLogsContains(
            expected_log, logger="debusine", level=logging.WARNING
        ):
            notifications._send_work_request_completed_notification(
                notification_channel,
                work_request,
                work_request_notification,
                body,
            )

        mocked.assert_called_once_with(fail_silently=True)
