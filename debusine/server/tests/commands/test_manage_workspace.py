# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the management command manage_workspace."""
from datetime import timedelta

from django.core.management import CommandError
from django.test import TestCase

from debusine.db.models import Workspace
from debusine.server.tests.commands import call_command


class ManageWorkspaceCommandTests(TestCase):
    """Tests for manage_workspace management command."""

    def setUp(self):
        """Create a default Workspace."""
        self.workspace = Workspace.objects.create_with_name(name="test")

    def test_no_default_reset(self):
        """Don't confuse no-option with reset-to-default."""
        # Non-default values
        default_expiration_delay = 7
        stdout, stderr, exit_code = call_command(
            "manage_workspace",
            self.workspace.name,
            "--public",
            f"--default-expiration-delay={default_expiration_delay}",
        )

        stdout, stderr, exit_code = call_command(
            "manage_workspace",
            self.workspace.name,
        )

        self.workspace.refresh_from_db()
        self.assertTrue(self.workspace.public)
        self.assertEqual(
            self.workspace.default_expiration_delay,
            timedelta(default_expiration_delay),
        )

    def test_change_public(self):
        """manage_workspace changes the public status."""
        for public in (True, False):
            stdout, stderr, exit_code = call_command(
                "manage_workspace",
                "--public" if public else "--private",
                self.workspace.name,
            )

            self.assertEqual(stdout, "")
            self.assertEqual(stderr, "")
            self.assertEqual(exit_code, 0)

            self.workspace.refresh_from_db()
            self.assertEqual(self.workspace.public, public)

    def test_change_public_not_found(self):
        """manage_workspace workspace not found."""
        name = "non-existing-workspace"

        expected_error = f'Workspace "{name}" not found'
        with self.assertRaisesRegex(CommandError, expected_error) as exc:
            call_command(
                "manage_workspace",
                "--public",
                name,
            )

        self.assertEqual(exc.exception.returncode, 3)

    def test_change_default_expiration_delay(self):
        """manage_workspace changes the default expiration delay."""
        default_expiration_delay = 123456

        with self.assertRaises(CommandError):
            call_command(
                "manage_workspace",
                "--default-expiration-delay",
                "letters",
                self.workspace.name,
            )

        stdout, stderr, exit_code = call_command(
            "manage_workspace",
            "--default-expiration-delay",
            default_expiration_delay,
            self.workspace.name,
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        self.workspace.refresh_from_db()
        self.assertEqual(
            self.workspace.default_expiration_delay,
            timedelta(default_expiration_delay),
        )

        # Ensure "0" isn't confused with "no value given"
        default_expiration_delay = 0

        call_command(
            "manage_workspace",
            "--default-expiration-delay",
            default_expiration_delay,
            self.workspace.name,
        )

        self.workspace.refresh_from_db()
        self.assertEqual(
            self.workspace.default_expiration_delay,
            timedelta(default_expiration_delay),
        )

    def test_change_all(self):
        """manage_workspace changes multiple fields at once."""
        default_expiration_delay = 7
        stdout, stderr, exit_code = call_command(
            "manage_workspace",
            "--public",
            f"--default-expiration-delay={default_expiration_delay}",
            self.workspace.name,
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        self.workspace.refresh_from_db()
        self.assertTrue(self.workspace.public)
        self.assertEqual(
            self.workspace.default_expiration_delay,
            timedelta(default_expiration_delay),
        )
