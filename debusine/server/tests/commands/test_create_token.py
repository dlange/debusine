# Copyright 2021-2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the management command create_token."""

from django.contrib.auth import get_user_model
from django.core.management import CommandError
from django.test import TestCase

from debusine.db.models import Token
from debusine.server.tests.commands import call_command


class CreateTokenCommandTests(TestCase):
    """Tests for the create_token command."""

    def test_create_token(self):
        """create_token creates a new token and prints the key on stdout."""
        username = "james"
        get_user_model().objects.create_user(username=username, password="1234")
        stdout, stderr, exit_code = call_command('create_token', username)

        token = Token.objects.first()

        self.assertEqual(stdout, f'{token.key}\n')
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)
        self.assertTrue(token.enabled)

    def test_username_does_not_exist(self):
        """create_token cannot create a token: username does not exist."""
        username = "james"
        expected_error = f'Cannot create token: "{username}" does not exist'
        with self.assertRaisesRegex(CommandError, expected_error) as exc:
            call_command("create_token", username)

        self.assertEqual(exc.exception.returncode, 3)
        self.assertEqual(Token.objects.all().count(), 0)
