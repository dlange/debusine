# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Server-side Debusine tasks.

The tasks in this package run in a Celery worker.
"""

from debusine.server.tasks.base import BaseInternalTask

# Sub-tasks need to be imported in order to be available to BaseTask
# (e.g. for BaseTask.is_valid_task_name). They are registered via
# BaseTask.__init_subclass__.
from debusine.server.tasks.noop import InternalNoop  # noqa: F401, I202

__all__ = [
    "BaseInternalTask",
]
