# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Server-side no-operation task."""

from debusine.server.tasks import BaseInternalTask
from debusine.server.tasks.models import InternalNoopData


class InternalNoop(BaseInternalTask[InternalNoopData]):
    """Task that runs on internal workers and returns a boolean."""

    TASK_VERSION = 1

    def execute(self) -> bool:
        """Act as specified by the client."""
        if self.data.exception:
            raise RuntimeError("Client requested an exception")
        return self.data.result
