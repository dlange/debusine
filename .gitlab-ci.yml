---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml

# Extend the stages defined by salsa-ci
stages:
  - upstream-tests  # Added to run upstream tests first
  - provisioning
  - build
  - publish
  - test
  - deploy  # Added to deploy the pages

variables:
  RELEASE: 'bookworm'
  SALSA_CI_DISABLE_BUILD_PACKAGE_I386: 1
  SALSA_CI_DISABLE_APTLY: 0
  SALSA_CI_AUTOPKGTEST_ALLOWED_EXIT_STATUS: '0,2'

## Traditional upstream tests on top of salsa-ci
image: 'debian:bookworm'

.prepare-test: &prepare-test
  - export LANG=C.UTF-8
  - useradd debusine-test
  - su - debusine-test -c "NON_INTERACTIVE=1 bin/quick-setup.sh create_directories"
  - apt-get update

.before-test: &before-test
  - *prepare-test
  # Runtime dependencies
  - NON_INTERACTIVE=1 bin/quick-setup.sh install_packages

documentation-linting:
  stage: upstream-tests
  script:
    - *before-test
    # Generate documentation
    - make -C docs html O="-w _build/warnings.txt"
    - rm -rf docs/_build/doctrees  # Save space in generated artifact
    - "! test -s docs/_build/warnings.txt || (echo 'ERROR: Sphinx generated warnings:' >&2; cat docs/_build/warnings.txt >&2; exit 1)"
    - make -C docs linkcheck
  artifacts:
    expose_as: "generated documentation"
    paths:
      - docs/_build/html/index.html  # Single out index.html for the benefit of expose_as
      - docs/_build
    when: always
    expire_in: 2 weeks
  allow_failure: true

code-linting:
  stage: upstream-tests
  dependencies: []
  script:
    - *before-test
    - apt-get install -y git libpq-dev
    - make check
  except:
    - schedules

migrations:
  stage: upstream-tests
  dependencies: []
  script:
    - *before-test
    - service postgresql start || true
    - su - postgres -c "createuser -d root" || true
    - su - postgres -c "createdb --owner root debusine"
    - python3 manage.py migrate
    - python3 manage.py makemigrations --check --dry-run

.unit-tests-shared: &unit-tests-shared
  stage: upstream-tests
  dependencies: []
  except:
    - schedules

.unit-tests-coverage: &unit-tests-coverage
  coverage: '/TOTAL COVERAGE.* (\d+(?:\.\d+)?%)/'
  artifacts:
    expose_as: 'test coverage report'
    paths:
      - coverage/html/index.html
      - coverage
    when: always
    expire_in: 2 weeks

.unit-tests-shared-prepare: &unit-tests-shared-prepare
  - service postgresql start || true
  - service redis-server start
  - su - postgres -c "createuser -d debusine-test" || true

.unit-tests-shared-report: &unit-tests-shared-report
  - python3 -m coverage report -m | sed -e 's/^TOTAL         /TOTAL COVERAGE/'
  - service postgresql stop || true

unit-tests-bullseye:
  <<: *unit-tests-shared
  image: "debian:bullseye-backports"
  variables:
    RELEASE: "bullseye-backports"
  script:
    - *prepare-test
    - apt-get -y install python3-django/bullseye-backports
    - apt-get -y install ./ci/deb/bullseye/*.deb
    - NON_INTERACTIVE=1 bin/quick-setup.sh install_packages
    - *unit-tests-shared-prepare
    - su - debusine-test -c "make coverage VERBOSE=1"
    - *unit-tests-shared-report

unit-tests-trixie:
  <<: *unit-tests-shared
  image: "debian:trixie"
  variables:
    RELEASE: "trixie"
  script:
    - *prepare-test
    - apt-get -y install python3-django
    - NON_INTERACTIVE=1 bin/quick-setup.sh install_packages
    - *unit-tests-shared-prepare
    - su - debusine-test -c "make coverage VERBOSE=1"
    - *unit-tests-shared-report
  allow_failure: true

unit-tests:
  <<: *unit-tests-shared
  <<: *unit-tests-coverage
  script:
    - *before-test
    - *unit-tests-shared-prepare
    - su - debusine-test -c "make coverage VERBOSE=1"
    - *unit-tests-shared-report
    - "(python3 -m coverage report -m | grep --silent '^TOTAL.*100%$') || (echo 'ERROR: Coverage must be 100%' >&2 ; exit 1)"
    - "(python3 -c 'from setuptools import setup; setup()' check --strict) || (echo 'ERROR: setup.cfg has errors' >&2 ; exit 1)"

unit-tests-pip:
  <<: *unit-tests-shared
  script:
    - *prepare-test
    - apt-get install -y python3-venv sensible-utils chromium-driver postgresql postgresql-client redis-server graphviz make
    - python3 -m venv venv
    - . venv/bin/activate
    - sed -i 's/psycopg2/&-binary/' setup.cfg
    - pip install -e .[server,tests] coverage
    - *unit-tests-shared-prepare
    - su - debusine-test -c ". venv/bin/activate; make coverage VERBOSE=1"
    - *unit-tests-shared-report

autopkgtest:
  extends: .test-autopkgtest
  parallel:
    matrix:
      - SALSA_CI_AUTOPKGTEST_ARGS:
          - "--test-name=unit-tests-tasks --test-name=unit-tests-client --test-name=unit-tests-worker --test-name=unit-tests-server"
          - "--test-name=integration-tests-generic"
          - "--test-name=integration-tests-tasks-mmdebstrap-autopkgtest-sbuild-lintian-piuparts-blhc"

autopkgtest-bullseye:
  extends: .test-autopkgtest
  variables:
    RELEASE: "bullseye-backports"
  parallel:
    matrix:
      - SALSA_CI_AUTOPKGTEST_ARGS:
          - "--setup-commands=ci/pin-django-from-backports.sh --test-name=unit-tests-tasks --test-name=unit-tests-client --test-name=unit-tests-worker --test-name=unit-tests-server"
          # skip integration tests that require unshare
          - "--setup-commands=ci/pin-django-from-backports.sh --test-name=integration-tests-generic"
  allow_failure: true

autopkgtest-trixie:
  extends: .test-autopkgtest
  variables:
    RELEASE: "trixie"
  parallel:
    matrix:
      - SALSA_CI_AUTOPKGTEST_ARGS:
          - "--test-name=unit-tests-tasks --test-name=unit-tests-client --test-name=unit-tests-worker --test-name=unit-tests-server"
          - "--test-name=integration-tests-generic"
          - "--test-name=integration-tests-tasks-mmdebstrap-autopkgtest-sbuild-lintian-piuparts-blhc"
  allow_failure: true

.autopkgtest-simplesystemimagebuild:
  extends: .test-autopkgtest
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - changes:
        - debian/tests/integration-tests-task-simplesystemimagebuild
        - debian/tests/integration-tests-task-simplesystemimagebuild.py
        - debusine/tasks/simplesystemimagebuild.py
        - debusine/tasks/systemimagebuild.py
  variables:
    SALSA_CI_AUTOPKGTEST_ARGS: "--test-name=integration-tests-task-simplesystemimagebuild"

autopkgtest-simplesystemimagebuild-bookworm:
  extends: .autopkgtest-simplesystemimagebuild
  variables:
    RELEASE: "bookworm"

autopkgtest-simplesystemimagebuild-trixie:
  extends: .autopkgtest-simplesystemimagebuild
  variables:
    RELEASE: "trixie"
  allow_failure: true

pages:
  stage: deploy
  dependencies:
    - unit-tests             # To retrieve .coverage artifact
    - aptly                  # To retrieve the apt repository
    - documentation-linting  # To retrieve the documentation
  script:
    # Move documentation in the public space
    - mv docs/_build/html/ public/
    # Move coverage report in the public space
    - mv coverage/html public/coverage
    # Copy the repository created by aptly
    - (mkdir public/repository && cd aptly && cp -rfv . ../public/repository)
  artifacts:
    paths:
      - public
  only:
    refs:
      - devel
    variables:
      # Don't publish the documentation in random forks
      - $CI_PROJECT_PATH == "freexian-team/debusine" || $CI_PROJECT_PATH == "rhertzog/debusine"
  except:
    - schedules
