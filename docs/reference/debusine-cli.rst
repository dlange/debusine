.. _debusine-cli:

====================
The debusine command
====================

The ``debusine`` command is a command line interface to the debusine API.
It is provided by the ``debusine-client`` package and contains many
sub-commands.

Output of the ``debusine`` command
----------------------------------
If the ``debusine`` command succeeds, it prints relevant information to the
standard output in YAML format.

If an error occurs, the error messages will be printed to the standard error.

Return values
-------------

Return values of the ``debusine`` command:

===============  ==================================================================================
  Return value    Meaning
===============  ==================================================================================
 0                Success
 1                Error: unhandled exception. Please report the error
 2                Error: wrong arguments and options
 3                Error: any other type of error such as connection to the server is not possible,

                  invalid configuration file, etc.
===============  ==================================================================================

Sub-commands
------------

.. todo::

   Document all the subcommands.

.. code-block:: console

    $ debusine --help
    usage: debusine [-h] [--server SERVER] [--config-file CONFIG_FILE] [-s] [-d]
                    {list-work-requests,show-work-request,create-work-request,create-artifact,import-debian-artifact,download-artifact,show-artifact,on-work-request-completed}
                    ...

    Interacts with a debusine server.

    positional arguments:
      {list-work-requests,show-work-request,create-work-request,create-artifact,import-debian-artifact,download-artifact,show-artifact,on-work-request-completed}
                            Sub command
        list-work-requests  List all work requests
        show-work-request   Print the status of a work request
        create-work-request
                            Create a work request and schedule the execution. Work request is read from
                            stdin in YAML format
        create-artifact     Create an artifact
        import-debian-artifact
                            Import a Debian source/binary package
        download-artifact   Download an artifact in .tar.gz format
        show-artifact       Show artifact information
        on-work-request-completed
                            Execute a command when a work request is completed. Arguments to the command:
                            WorkRequest.id and WorkRequest.result
