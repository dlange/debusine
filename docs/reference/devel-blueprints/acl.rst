====================
Access Control Rules
====================

Currently debusine doesn't have any ACL and all authenticated users
can perform all operations in all workspaces. We expect to build access
control rules tied to the workspaces.

Each workspace would have a set of users that can have 3 different levels
of access:

* read-only access: can access all objects within the workspace but not
  make any change
* upload-only access: same as read-only but can create new artifacts and
  can modify their own artifacts.
* read-write access: can access all objects within the workspace and
  modify them, even those created by others.
* admin access: same as read-write and can also add/remove users to the
  workspace, and change generic properties of the workspace itself

To properly scale we might need something more advanced with
"organizations" grouping "users" and managing "groups" with groups being
granted rights to different workspaces.
