==============
APTMirror task
==============

This is a :ref:`server-side task <explanation-tasks>` that updates an
existing :ref:`debian:suite collection <collection-suite>` to reflect the
state of an external APT suite. It creates source and binary package
artifacts as required.

The ``task_data`` for this task may contain the following keys:

* ``url`` (required): the base URL of the external suite
* ``components`` (optional): if set, only mirror these components
* ``architectures`` (optional): if set, only mirror binary packages for this
  list of architectures
* ``signing_key`` (optional): ASCII-armored public key used to authenticate
  this suite

The suite must have at least a ``Release`` file to make it possible to
handle multiple architectures in a reasonable way, and if ``signing_key`` is
specified then it must also have either an ``InRelease`` or a
``Release.gpg`` file.

Additions and removals of collection items are timestamped as described in
:ref:`explanation-collections`, so tasks that need a static view of a
collection (e.g. so that all tasks in the same workflow instance see the
same base suite contents) can do this by filtering on collection items that
were created before or at a given point in time and were not yet removed at
that point in time.

.. todo::

   Document exactly how transactional updates of collections work in
   general: tasks need to see a coherent state, and simple updates to
   collections can be done in a database transaction, but some longer update
   tasks where using a single database transaction is impractical may need
   more careful handling.  See discussion in `!517
   <https://salsa.debian.org/freexian-team/debusine/-/merge_requests/517>`_.
