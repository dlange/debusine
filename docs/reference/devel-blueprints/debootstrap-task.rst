================
Debootstrap task
================

The ``debootstrap`` task implements the :ref:`SystemBootstrap
<system-bootstrap-task>` interface except that it only supports a single
repository in the ``bootstrap_repositories`` key.

On top of the keys defined in that interface, it also supports the
following additional keys in ``task_data``:

* ``bootstrap_options``

  * ``script``: last parameter on debootstrap's command line

The various keys in the first entry of ``bootstrap_repositories`` are mapped to the
corresponding command line options and parameters:

* ``mirror``, ``suite`` and ``script`` map to positional command line parameters
* ``components`` maps to ``--components``
* ``check_signature`` maps to ``--check-gpg`` or ``--no-check-gpg``
* ``keyring_package`` maps to an extra package name in ``--include``
* ``keyring`` maps to ``--keyring``

The following keys from ``bootstrap_options`` are also mapped:
* ``variant`` maps to ``--variant``
* ``extra_packages`` maps to ``--include``
