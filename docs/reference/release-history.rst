.. _release-history:

===============
Release history
===============

0.2.1 (2024-03-07)
------------------

Server
~~~~~~

* Add a Celery worker for server-side tasks.

Client
~~~~~~

* Trim down dependencies slightly.

Worker
~~~~~~

* Require KVM access for ``simplesystemimagebuild`` task.
* Change ``container`` to ``instance`` in Incus templates.
* Log task completion.
* Restrict tasks to workers that support the requested architecture.

Documentation
~~~~~~~~~~~~~

* Improve home page slightly.

Quality
~~~~~~~

* Enforce mypy project-wide, including all Django components.

0.2.0 (2024-02-29)
------------------

Highlights:

* Added artifact file storage system.
* Debian developers can use Debusine to run various QA tasks against
  packages they are preparing.  Those tasks can be scheduled through the API
  or through the web UI.

Note that it is not possible to directly migrate a database installed using
0.1.0.  Migrations from this release to future releases will be possible.

Server
~~~~~~

* Implement file storage.
* Implement artifact handling.
* Implement expiration of artifacts and their files.
* Run database migrations on ``debusine-server`` package upgrade.
* Add ``debusine-admin monthly_cleanup`` command, run from a systemd timer.
* Link work requests to workspaces.
* Add ``debusine-admin create_user``, ``debusine-admin list_users``, and
  ``debusine-admin manage_user`` commands.
* Link tokens to users.
* Allow email notifications if a work request fails.
* Depend on ``python3-daphne``.
* Ensure all local artifacts are JSON-serializable.
* Add ``debusine-admin create_workspace``, ``debusine-admin
  list_workspaces``, and ``debusine-admin manage_workspace`` commands.
* Use WorkRequest workspace in artifacts.
* Add default expiration delay to workspaces.
* Add API to list work requests.
* Make sure the Django app's secret key is never publicly readable.
* Mark workers as disconnected on ``debusine-server`` startup.
* Use ``Restart=on-failure`` rather than ``Restart=always`` in
  ``debusine-server.service``.
* Add ``debusine-admin info`` command to help with setting up deployments.
* Add daily artifact cleanup timer.
* Use pydantic models for artifact data.
* Add remote, read-only file storage backend for external Debian archives.

Web UI
~~~~~~

* Add web UI for work requests and workspaces.
* Add login/logout support to web UI, allowing access to non-public
  workspaces.
* Allow registering/removing user API keys using the web UI.
* Allow uploading artifacts using the web UI.
* Refinements to web UI for work requests.
* Make Django aware of HTTP/HTTPS state of requests.
* Fix download error with empty artifact file and document mmap usage.
* Implement integration with Salsa Single Sign-On.
* Add ``lintian`` view.
* Polish various aspects of the web UI.
* Add ``autopkgtest`` view.
* Fetch images for tasks directly, not via a tarball.

Client
~~~~~~

* Rename client's configuration key from ``debusine`` to ``api-url``.
* Add ``--data`` option to ``debusine create-work-request``.
* Rename ``debusine work-request-status`` to ``debusine show-work-request``.
* Add ``debusine on-work-request-completed`` to allow running a command when
  a work request completes.
* ``debusine.client``: Drop obsolete ``silent`` keyword, and stricter
  prototype tests.
* Add ``debusine --debug`` option to debug HTTP traffic.
* Implement a package downloader (``dget``).
* Implement a paginated listing API client.
* Add API client method for listing all work requests.
* Add ``debusine list-work-requests`` command.
* Add ``debusine import-debian-artifact`` command.

Worker
~~~~~~

* Modify ``sbuild`` task to use artifacts.
* Add pre-upload consistency checks on sbuild results.
* Rename worker's configuration key from ``debusine-url`` to ``api-url``.
* Upload ``sbuild`` log files even if the .dsc file did not exist.
* Add ``piuparts`` task.
* Add ``lintian`` task.
* Add ``autopkgtest`` task.
* Add ``mmdebstrap`` task.
* Avoid trying to add ``debusine-worker`` user in postinst if it already
  exists.
* Add image caching for executor backends.
* Add ``unshare`` executor.
* Port the ``autopkgtest`` and ``piuparts`` tasks to ``unshare``.
* Use ``Restart=on-failure`` rather than ``Restart=always`` in
  ``debusine-worker.service``.
* Make tasks check whether their tools are installed.
* Use a lock to protect execution of the work request.
* Add ``blhc`` task.
* Add ``simplesystemimagebuild`` task.
* Use pydantic models for task data.
* Log exceptions in task preparation and clean-up.
* Add Incus executor (for both containers and VMs).
* Add a ``qemu`` executor, currently only for ``autopkgtest`` and ``sbuild``
  tasks.

Documentation
~~~~~~~~~~~~~

* Drop the "slug" field and the "repository" type.
* Document ``debian:package-build-log`` artifact in ontology.
* Document using ``local.py`` to change settings.
* Create an overview document with an elevator-pitch-style introduction.
* Add initial design for ``autopkgtest`` and ``lintian`` tasks.
* Add initial design for system tarball artifacts and debootstrap-like
  tasks.
* Add initial design for tasks building system disk images.
* Update the description of the ``sbuild`` task.
* Restructure the documentation following the Diátaxis principles.
* Clarify copyright notice, contributor status and list of contributors.
* Enable the Sphinx copybutton plugin.
* Add some documentation for the Python client API.
* Improve the "Getting started with debusine" tutorial.
* Add documentation for ``debusine-admin`` commands.
* Add "Install your first debusine instance" tutorial.
* Add initial design for collections.
* Refine design for workflows.

Quality
~~~~~~~

* Harmonize license to be GPL-3+ everywhere.
* Support pydantic 1 and 2.
* Apply mypy, pyupgrade, and shellcheck consistently.
* Sync ``(Build-)Depends`` with ``setup.cfg``.

0.1.0 (2022-09-09)
------------------

Initial release.  Includes a server that can drive many workers over a
worker-initiated websocket connection, where the workers use the server's
API to get work requests and provide results.  There is an ``sbuild`` task
that workers can run.
