.. _add-new-worker:

================
Add a new worker
================

Adding a new worker is a two-step process: first you need to :ref:`setup
the debusine worker <set-up-debusine-worker>` on some server, and then you
need to enable the worker on the debusine server.

Once configured, the worker automatically tries to connect to the debusine
server, which initially denies him access because it is unknown. There are
two ways to enable the worker on the debusine server:

Enable a new worker on the server based on the worker's name
------------------------------------------------------------

The Debusine admin should find the worker, for example:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_workers

An output example::

  Name        Registered                        Connected                         Token                                                             Enabled
  ----------  --------------------------------  --------------------------------  ----------------------------------------------------------------  ---------
  saturn      2022-06-13T09:10:21.143146+00:00  2022-06-13T10:26:09.184059+00:00  627ee89ee9b7157295c3acd0461d179ee260eaf4244b82216de3b1a502a15658  True
  mars        2022-06-13T14:25:47.727794+00:00  2022-06-16T07:54:44.288758+00:00  6d5baf211fd96920650495391058bb43e7eb6e12b7510c4fa39c0ac1abf6755e  True
  mercury     2022-06-16T07:54:42.139968+00:00  -                                 93f1b0e9ad2e0ad1fd550f5fdf4ab809594fe1b38ffd37c5b3aa3858062ce0ab  False

In this case, the worker ``mercury`` is the newest addition according to the available
information: it has the latest registered time and is the only one that is not enabled.

The default name of the worker is the FQDN of the worker.

To enable the new worker on the server:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_worker enable mercury

The worker then will be able to connect and process tasks.

Enable a new worker on the server based on the token
----------------------------------------------------

It is also possible for the debusine-worker admin to send the token associated
to the worker, to the debusine-server admin. This would allow the
debusine-server admin to enable the worker.

The token can be found in the worker's installation system in the file
``/etc/debusine/worker/token`` or in the logs
(by default, it is in ``/var/log/debusine/worker/worker.log``).

The debusine-server admin could then enable the token using the command:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_token enable 93f1b0e9ad2e0ad1fd550f5fdf4ab809594fe1b38ffd37c5b3aa3858062ce0ab
