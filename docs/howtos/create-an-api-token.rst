.. _create-api-token:

===================
Create an API token
===================

As a debusine user
------------------

If you have regular user access to debusine, you need to connect to the web
interface, authenticate yourself, click on "Tokens" on your user menu at
the top right of the page, then follow the instructions to create a new token.

You can enter a comment to document the expected use of the token and
decide if you want to enable it immediately or not. After submission,
you are back to the list of tokens and you can click on "Show / Hide
token" to reveal the generated token.

As a debusine administrator
---------------------------

If you are a debusine administrator, then you can rely on the
ref:`debusine-admin <debusine-admin-cli>` command to create a new token
for any user:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin create_token john-smith --comment "Testing Debusine"

The token will be printed to the standard output. The token can then be sent to
the user.

Next steps
----------

The token will generally be used to :ref:`configure the debusine client
<set-up-debusine-client>`.
